<?php

use App\Http\Controllers\Dashboard\PostController;
use App\Http\Controllers\Dashboard\TestController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//ruta de tipo Recurso
Route::resource('post', PostController::class);

//nos ahorramos estos
/*
Route::get('post', PostController::class, 'index');
Route::get('post/{post}', PostController::class, 'show');
Route::get('post/create', PostController::class, 'create');
Route::get('post/post/edit', PostController::class, 'edit');


Route::post('post', [PostController::class, 'store']);
Route::put('post/{post}', [PostController::class, 'update']);
Route::delete('post/{post}', [PostController::class, 'delete']);
*/
/* Practicando
Route::get('/',[TestController::class, 'index']);

//Route::get('/test', [TestController::class, 'test']);

Route::get('/contacto', function() {
  return view('contacto');
})->name('contacto');


//como enviar parametrod
Route::get('/custom', function() {
  $mensaje = "Mensaje del servidor :)";
  $data = ['msj' => $mensaje, 'edad' => 15];
  //indicar la clave valor
  return view('custom', $data);
});
*/
