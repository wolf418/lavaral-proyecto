
<!--llamar al formato madre -->

@extends('dashboard/layout')

<!--colocar las seccion-->
@section('content')

  <div class="container">
      <h1>Crear Post</h1>

    @include('dashboard/fragment.errors-form')

    <form class="" action="{{ route('post.store') }}" method="post">
      <!--proteccion csrh-->
      @csrf
      <div class="">
        <label for="">Titulo</label>
        <input type="text" name="title" value="{{ old("title","") }}">
      </div>
      <div class="">
        <label for="">Slug</label>
        <input type="text" name="slug" value=" {{old("slug","")}} ">
      </div>
      <div class="">
        <label for="">Seleccione Categoria: </label>
        <select class="" name="categoria_id">
            <option value="">Seleccione:</option>

           @foreach ($categorias as $titulo => $id)
           <option {{ old("categoria_id","") == $id ? 'selected' : '' }} value="{{ $id }}">{{ $titulo }}</option>

            @endforeach
        </select>
      </div>
      <div class="">
        <label for="">Va a publicarlo: ?</label>
        <select class="" name="posted">
          <option {{ old("posted","") == "not" ? "selected" : ""}} value="not">No</option>
          <option {{ old("posted","") == "yes" ? "selected" : ""}} value="yes">Si</option>
        </select>

      </div>
      <div class="">
        <label for="">Contenido</label>
        <textarea name="content" rows="8"> {{ old("content","") }} </textarea>
      </div>
      <div class="">
        <label for="">Descripcion</label>
        <textarea name="description" rows="8">{{ old("description","") }}</textarea>
      </div>

      <button type="submit" name="button" class="button">Enviar</button>
    </form>
  </div>

@endsection
