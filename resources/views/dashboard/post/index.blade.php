
@extends('dashboard/layout')

@section('content')

  <h1>Hola mundo index - Post</h1>
    <div class="">
        <div class="">
            <div class="">
              <a href="{{route("post.create")}}">Crear</a>
                <table>
                  <thead>
                    <tr>
                      <th>Titulo</th>
                      <th>Categoria</th>
                      <th>Postedado</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($posts as $post)
                    <tr>
                      <td>{{ $post->title }}</td>
                      <td>Categoria</td>
                      <td>{{ $post->posted }}</td>
                      <td>
                        <a href="{{ route("post.edit", $post->id) }}">Editar</a>
                        <a href="{{ route("post.show", $post->id) }}">Ver</a>
                        <form class="" action="{{ route("post.destroy", $post->id) }}" method="post">
                          <!--directiva laravel para que funcione el destroy-->
                          @method("DELETE")
                          @csrf
                            <button type="submit" name="button">Eliminar</button>
                        </form>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                <!--funcion del paginado-->
                {{ $posts->links() }}
            </div>
        </div>
    </div>
@endsection
