
<!--llamar al formato madre -->

@extends('dashboard/layout')

<!--colocar las seccion-->
@section('content')

  <div class="container">
      <h1>Actualizar Post: {{ $post->title }}</h1>

    @include('dashboard/fragment.errors-form')

    <form class="" action="{{ route('post.update', $post->id) }}" method="post">
      <!--proteccion csrh-->
      @csrf
      <!--Agregando el metodo para una correcta interpretacion de laravel-->
      @method("PUT")
      <div class="">
        <label for="">Titulo</label>
        <input type="text" name="title" value="{{ $post->title }}">
      </div>
      <div class="">
        <label for="">Slug</label>
        <input readonly type="text" name="slug" value="{{ $post->slug }}">
      </div>
      <div class="">
        <label for="">Seleccione Categoria: </label>
        <select class="" name="categoria_id">
            <option value="">Seleccione:</option>

           @foreach ($categorias as $titulo => $id)
           <option {{ $post->categoria_id == $id ? 'selected' : '' }} value="{{ $id }}">{{ $titulo }}</option>

            @endforeach
        </select>
      </div>
      <div class="">
        <label for="">Va a publicarlo: ?</label>
        <select class="" name="posted">
          <option {{ $post->posted == 'not' ? 'selected' : '' }} value="not">No</option>
          <option {{ $post->posted == 'yes' ? 'selected' : '' }} value="yes">Si</option>
        </select>

      </div>
      <div class="">
        <label for="">Contenido</label>
        <textarea name="content" rows="8">{{ $post->content }}</textarea>
      </div>
      <div class="">
        <label for="">Descripcion</label>
        <textarea name="description" rows="8">{{ $post->description }}</textarea>
      </div>

      <button type="submit" name="button" class="button">Enviar</button>
    </form>
  </div>

@endsection
