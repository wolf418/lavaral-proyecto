<!--Funcionalidad de laravel para mostrar errores -->
@if ($errors->any())
  @foreach ($errors->all() as $error)
    <div class="error">
        {{ $error }}
    </div>
  @endforeach
@endif
