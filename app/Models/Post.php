<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    //definiendo la propiedad fillable, para evitar la inyeccion de
    //atributos no deseados, limpieza de los campos
    protected $fillable = ['title', 'slug','content', 'categoria_id', 'description', 'posted', 'image'];

}
