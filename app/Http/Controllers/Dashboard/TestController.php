<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TestController extends Controller
{
    /*
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //echo "Hola mundo";
        //return view('dashboard.test.index', ['name' => "Jhonathan", 'age' => 28 ,'html' => '<h1>Titulo</h1>', 'array' => [1,2,3,4,5,'arreglo-foreach'], 'users' => [10,11,12,13,14,15,'arreglo-forelse']]);
        $posts = [1,2,3,4,'jhonathan'];
        $users = [10,11,12,13,14,15,'arreglo-forelse'];
        //return view('dashboard.test.index', ['posts' => $posts, 'users' => [10,11,12,13,14,15,'arreglo-forelse']]);
        //organizar de una mejor manera
        //return view('dashboard.test.index', ['posts' => $posts, 'users' => $users]);
        //con la funcion compact
        return view('dashboard.test.index', compact('posts','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
