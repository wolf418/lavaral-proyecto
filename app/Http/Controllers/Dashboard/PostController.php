<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\StorePostRequest;
use App\Http\Requests\Post\UpdatePutRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\Post;
use App\Models\Categoria;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //echo "hola index - post";
        $posts = Post::paginate(2);
        //echo view('dashboard/post.index', ["posts" => $posts]);
        return view('dashboard/post.index', ["posts" => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = Categoria::pluck('id', 'titulo');
        //dd($categorias);
        echo view('dashboard/post.create', compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        //
        //visualizar datos de forma amigable
        //dd($request);
        //dd(request("title"));
        //echo request('title');
        //echo $request->input('slug');
        //mostrar todos los datos del form
        //dd($request->all());
        //insertar datos Post::created($request->all());
        //insertar datos faltando un campo
        /*
        $data = array_merge($request->all(), ['image' => '']);
        dd($data);
        Post::created($data);*/
        //Validar la data, mediante el request de StorePostRequest
        //donde se difinen las reglas
        //otra forma
        //$request->validate(StorePostRequest::rules())
      //  Post::created($request->all());


        Post::create($request->validated());


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
        echo "show";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categorias = Categoria::pluck('id', 'titulo');
        echo view('dashboard/post.edit', compact('categorias', 'post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePutRequest $request, Post $post)
    {
        //dd($request->validated());
        //instancia de clase
        $post->update($request->validated());

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        echo "destroy";
    }
}
