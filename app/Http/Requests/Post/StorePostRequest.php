<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
      //se establece true para autorizar las validaciones
        return true;
    }


    protected function prepareForValidation()
    {
      //el metodo permite manipular el request, antes de hacer las validaciones
      //la funcion debe de ser prepareForValidation
       $this->merge([
         //agrega el seperador ala url => crea una url limpia
         //'slug' => Str::slug($this->title)
         //para encadenar
         //'slug' => Str::of($this->title)->slug()->append("-adicional")
         'slug' => Str::of($this->title)->slug()
       ]);

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    static public function rules()
    {
        return [
          "title" => "required|min:5|max:255",
          "description" => "required|min:10",
          "slug" => "required|min:5|max:255|unique:posts",
          "content" => "required|min:10",
          "posted" => "required",
          "categoria_id" => "required|integer"
        ];
    }
}
