<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string("title", 255)->nullable();
            $table->text("description")->nullable();
            $table->string("slug", 255)->nullable();
            $table->text("content")->nullable();
            $table->string("image")->nullable();
            $table->enum("posted", ['yes', 'not'])->default('not');
            //enlazar llaves foraneas
            //$table->foreignId("category_id")->constrained("categorias")->onDelete("cascade");
            $table->unsignedBigInteger('categoria_id');
            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
};
